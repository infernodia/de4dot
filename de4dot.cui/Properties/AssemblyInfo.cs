﻿/*
    Copyright (C) 2011-2013 de4dot@gmail.com

    This file is part of de4dot.

    de4dot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    de4dot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with de4dot.  If not, see <http://www.gnu.org/licenses/>.
*/

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("de4dot.cui")]
[assembly: AssemblyDescription("de4dot Console UI code")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("de4dot.cui")]
[assembly: AssemblyCopyright("Copyright (C) 2011-2013 de4dot@gmail.com")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("2.0.3.3405")]
[assembly: AssemblyFileVersion("2.0.3.3405")]
[assembly: InternalsVisibleTo("de4dot, PublicKey=0024000004800000940000000602000000240000525341310004000001000100f54e55d9dfca1d24f7e9a2a7e6467441425a2fd03f40a49c7973d6be077bcfe35df4b9374859c28a26f837987f4b728b03afdcc443090d560395166d1770a43cfc6e56a377a4c677ea5051a122cf420b008aafcfa713c6ef1be41ad7dbd0bf4f0794c9d5e3c577c6d25e67ec5ffc6c8739d8d5082e296b3ddef746e5e948aca4")]
[assembly: InternalsVisibleTo("de4dot-x64, PublicKey=0024000004800000940000000602000000240000525341310004000001000100f54e55d9dfca1d24f7e9a2a7e6467441425a2fd03f40a49c7973d6be077bcfe35df4b9374859c28a26f837987f4b728b03afdcc443090d560395166d1770a43cfc6e56a377a4c677ea5051a122cf420b008aafcfa713c6ef1be41ad7dbd0bf4f0794c9d5e3c577c6d25e67ec5ffc6c8739d8d5082e296b3ddef746e5e948aca4")]

